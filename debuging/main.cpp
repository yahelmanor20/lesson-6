#include <iostream>
using namespace std;
void first();
#define PI 3.14
int main()
{
	first();
	system("pause");
	return 0;
}
void first()
{
	unsigned int size = 0, i = 0;

	int t1 = 1, t2 = 1, nextTerm;

	cout << "what is the size of the series? ";
	cin >> size;
	i = size;
	while (i > 0)
	{
		nextTerm = t1 + t2;
		printf("%d, ", nextTerm);
		t1 = t2;
		t2 = nextTerm;
		i--;
	}
}
