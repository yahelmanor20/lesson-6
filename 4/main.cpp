#include <stdio.h>
#include <string.h>
#include <stdlib.h>


void memset_func(char* buff, int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		buff[i] = 0;
	}
	printf("after initializing buffer\n");
}

int main()
{
	char buffer[5];


	memset_func(buffer, 5);

	strcpy_s(buffer, "HELL");
	printf("buffer content= %s\n", buffer);

	system("pause");
	return 0;
}
