#pragma once
#include <math.h>
#define ANGEL 108
class MathUtils
{
public:
	MathUtils();
	~MathUtils();
	static double CalPentagonArea(int a);
	static double CalHexagonArea();
};

